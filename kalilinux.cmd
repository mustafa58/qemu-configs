@echo off
setlocal
qemu-system-x86_64.exe^
 -boot c^
 -hda C:\Users\Public\Documents\VMs\KaliLinux.img^
 -smp 4 -m 4G -accel whpx -L "C:\Program Files\qemu"^
 -usb -device usb-tablet -vga virtio -display sdl,gl=off^
 -device AC97