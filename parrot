#!/bin/sh

#export QEMU_AUDIO_DRV=alsa
export QEMU_PA_SAMPLES=8192

hugepages alloc 4G
qemu-system-x86_64 \
	-enable-kvm \
	-boot menu=on -M q35 \
	-drive file=Parrot.img,cache=writeback,aio=threads \
	-audiodev pa,id=snd0,server=/run/user/1000/pulse/native,out.fixed-settings=off,timer-period=99,out.buffer-length=4000 \
	-device ich9-intel-hda -device hda-micro,audiodev=snd0 \
	-m 4G -mem-prealloc -mem-path /dev/hugepages \
	-cpu host -smp 6,cores=3,threads=2 \
	-vga virtio -display sdl \
	-usb -device usb-tablet \
	-rtc base=utc \
	$@
hugepages free 4G
